#include "encryptionprovider.h"
#include <QDebug>


EncryptionProvider::EncryptionProvider()
{
    if (!acquireContext())
        qDebug() << "Не удалось получить контекст";
}

EncryptionProvider::EncryptionProvider(QString passphrase)
{
    if (!acquireContext())
        qDebug() << "Не удалось получить контекст";
    else
        setKey(passphrase);
}

EncryptionProvider::~EncryptionProvider()
{
    if(hKey)
        CryptDestroyKey(hKey);
    if(hContext)
        CryptReleaseContext(hContext, 0);
}

void EncryptionProvider::setKey(QString passphrase)
{
    if (!generateKey(passphrase.toUtf8()))
        qDebug() << "Не удалось сгенерировать ключ";
}

bool EncryptionProvider::encrypt(QByteArray &data)
{
    unsigned long len = data.length();
    unsigned long newLen = len;
    auto ok = CryptEncrypt(hKey, NULL, true, 0, NULL, &newLen, len);

    data.resize(newLen);

    CryptEncrypt(hKey, NULL, true, 0, (BYTE*)data.data(), &len, newLen);
}

bool EncryptionProvider::decrypt(QByteArray &data)
{
    unsigned long len = data.length();
    auto ok = CryptDecrypt(hKey, NULL, true, 0, (BYTE*)data.data(), &len);

    data.resize(len);
}

bool EncryptionProvider::acquireContext()
{
    return CryptAcquireContext(&hContext, NULL, NULL, PROV_RSA_AES, 0);
}

bool EncryptionProvider::generateKey(QByteArray key)
{
    HCRYPTHASH hash;
    if(!CryptCreateHash(hContext,CALG_SHA_256, NULL, 0, &hash))
    {
        qDebug() << "createHash" << GetLastError();
        return false;
    }

    if(!CryptHashData(hash, (const BYTE*)key.constData(), key.length(), 0))
    {
        qDebug() << "hashData" << GetLastError();
        return false;
    }

    if(!CryptDeriveKey(hContext, CALG_AES_128, hash, 0, &hKey))
    {
        qDebug() << "deriveKey" << GetLastError();
        return false;
    }

    CryptDestroyHash(hash);

    return true;
}
