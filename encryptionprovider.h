#ifndef ENCRYPTIONPROVIDER_H
#define ENCRYPTIONPROVIDER_H

#include <QString>
#include <windows.h>
#include <wincrypt.h>

class EncryptionProvider
{
public:
    EncryptionProvider();
    EncryptionProvider(QString passphrase);

    ~EncryptionProvider();

    void setKey(QString passphrase);
    bool encrypt(QByteArray &data);
    bool decrypt(QByteArray &data);

private:
    HCRYPTPROV hContext = NULL;
    HCRYPTKEY hKey = NULL;

    bool acquireContext();
    bool generateKey(QByteArray key);

};

#endif // ENCRYPTIONPROVIDER_H
