#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

#include "mediamanager.h"
#include "networkmanager.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    MediaManager *mm;
    NetworkManager *nm;
    QThread netwrokThread;

    void setupValidators();
    void disableAllButtons();
    void displayMesage(QString msg, bool own);

private slots:
    void validateInput();
    void onIncomingCall(QString ip);
    void onCallStarted();
    void onCallEnded();
    void onError(QString error);

    void onStartCallPressed();

    void onSendMessage();
    void onTextMessage(QString msg);

signals:
    void callStart(QString ip, QString password);
    void callEnd();
    void callAccept(QString password);
    void callDecline();
    void muteMic(bool);
    void muteSound(bool);
    void muteCam(bool);

    void textMessage(QString msg);

};
#endif // MAINWINDOW_H
