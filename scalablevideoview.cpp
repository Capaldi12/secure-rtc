#include "scalablevideoview.h"

#include <QDebug>

ScalableVideoView::ScalableVideoView(QWidget *parent)
    : QGraphicsView(parent)
{
    scene = new QGraphicsScene();
    scene->setBackgroundBrush(QBrush(Qt::black));

    setScene(scene);
}

void ScalableVideoView::setDisplayItem(QGraphicsPixmapItem *item)
{
    if (this->item)
        scene->removeItem(this->item);

    this->item = item;
    scene->addItem(item);
}

QGraphicsPixmapItem *ScalableVideoView::removeDisplayItem()
{
    if (item && scene)
    {
        scene->removeItem(item);
        auto ret = item;
        item = nullptr;

        return ret;
    }

    return nullptr;
}

void ScalableVideoView::adjustSize()
{
    resizeDisplayItem(size());
}

void ScalableVideoView::resizeDisplayItem(QSize vSize)
{
    // Set sccene bounding box to view box
    scene->setSceneRect(0, 0, vSize.width(), vSize.height());

    if (!item || item->pixmap().isNull()) return;

    QSize iSize = item->pixmap().size();

    // Rescale image to fit view

    // Calculate aspect ratio of item and view
    double iRatio = (double)iSize.width() / (double)iSize.height();
    double vRatio = (double)vSize.width() / (double)vSize.height();

    double scale;
    double x = 0, y = 0;

    if (iRatio <= vRatio)
    {
        // Set height ratio as scale factor
        scale = (double)vSize.height() / (double)iSize.height();

        // Move item to the middle horizontaly (it should fit exactly verticaly)
        x = (vSize.width() - scale * iSize.width()) / 2;
    }
    else
    {
        // Set width ratio as scale factor
        scale = (double)vSize.width() / (double)iSize.width();

        // Move item to the middle verticaly (it should fit exactly horizontaly)
        y = (vSize.height() - scale * iSize.height()) / 2;
    }

    // Change item scale factor and pos
    item->setScale(scale);
    item->setPos(x, y);
}

void ScalableVideoView::resizeEvent(QResizeEvent *event)
{
    resizeDisplayItem(event->size());

    QGraphicsView::resizeEvent(event);
}
