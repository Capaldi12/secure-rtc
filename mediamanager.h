#ifndef MEDIAMANAGER_H
#define MEDIAMANAGER_H

#include <QObject>
#include <QMainWindow>

#include <QCamera>
#include <QCameraImageCapture>
#include <QGraphicsPixmapItem>

#include <QAudioInput>
#include <QAudioOutput>
#include <QIODevice>
#include <QBuffer>
#include <QAudio>

#include "scalablevideoview.h"

class MediaManager : public QObject
{
    Q_OBJECT
public:
    explicit MediaManager(ScalableVideoView *view, QMainWindow *parent = nullptr);
    virtual ~MediaManager();

private:
    // Video
    ScalableVideoView *view = nullptr;    // View to display video in

    QCameraImageCapture *capture = nullptr;    // Video captured by camera
    QGraphicsPixmapItem *display = nullptr;    // Video to be displayed on screen

    QCamera *camera = nullptr;
    QCameraViewfinderSettings *cameraSettings = nullptr;

    // Audio
    QIODevice *audioCapture = nullptr;      // Buffer for input audio
    QIODevice *audioPlayback = nullptr;     // Buffer for output audio

    QAudioInput *microphone = nullptr;
    QAudioOutput *player = nullptr;

    // Media mute settings
    bool micMuted = true;
    bool soundMuted = true;
    bool camMuted = true;

public slots:
    // Mute toggles
    void onMuteMic(bool mute);
    void onMuteSound(bool mute);
    void onMuteCam(bool mute);

    // Data receiving
    void onNewVideoData(QImage data);
    void onNewAudioData(QByteArray data);

private slots:
    void onCameraStateChanged();
    void onReadyForCapture(bool state);
    void onImageCaptured(int id, const QImage &image);

    void onInputBufferWritten(qint64 bytes);
    void onOutputBufferReady();

signals:
    // New data to be sent
    void newVideoData(QImage data);
    void newAudioData(QByteArray data);

    // Sent when widget has something to say
    void StatusMessageRequested(QString msg, int timeout);
};

#endif // MEDIAMANAGER_H
