#include "mediamanager.h"

#include <QCameraInfo>
#include <QElapsedTimer>

MediaManager::MediaManager(ScalableVideoView *view, QMainWindow *parent)
    : QObject(parent)
{
    this->view = view;

    // Video ---------------------------
    // Just use default camera
    camera = new QCamera(QCameraInfo::defaultCamera(), this);

    // Set correct resolution when camera loaded
    connect(camera, &QCamera::stateChanged, this, &MediaManager::onCameraStateChanged);

    display = new QGraphicsPixmapItem();
    view->setDisplayItem(display);      // Set screen source

    camera->setCaptureMode(QCamera::CaptureStillImage);

    capture = new QCameraImageCapture(camera, this);
    capture->setCaptureDestination(QCameraImageCapture::CaptureToBuffer);

    connect(capture, &QCameraImageCapture::readyForCaptureChanged, this, &MediaManager::onReadyForCapture);
    connect(capture, &QCameraImageCapture::imageCaptured, this, &MediaManager::onImageCaptured);
    // ---------------------------------

    // Audio ---------------------------
    // Set desired audio format - magic numbers from docs
    QAudioFormat format;
    format.setSampleRate(8000);
    format.setChannelCount(1);
    format.setSampleSize(8);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::UnSignedInt);

    // Default IO devices
    auto iInfo = QAudioDeviceInfo::defaultInputDevice();
    auto oInfo = QAudioDeviceInfo::defaultOutputDevice();

    // Adjust format to devices
    format = iInfo.nearestFormat(format);
    format = oInfo.nearestFormat(format);

    // Instantiate devices
    microphone = new QAudioInput(iInfo, format);
    player = new QAudioOutput(oInfo, format);

    // Make buffers
    audioCapture = new QBuffer();
    audioCapture->open(QBuffer::ReadWrite);

    audioPlayback = new QBuffer();
    audioPlayback->open(QBuffer::ReadWrite);

    connect(audioCapture, &QBuffer::bytesWritten, this, &MediaManager::onInputBufferWritten);
    connect(audioPlayback, &QBuffer::readyRead, this, &MediaManager::onOutputBufferReady);
    // ---------------------------------
}

MediaManager::~MediaManager()
{
    delete display;
    delete capture;

    delete cameraSettings;
    delete camera;

    delete microphone;
    delete player;

    if (audioCapture && audioCapture->isOpen())
        audioCapture->close();

    if (audioPlayback && audioPlayback->isOpen())
        audioPlayback->close();

    delete audioCapture;
    delete audioPlayback;
}

void MediaManager::onMuteMic(bool mute)
{
    micMuted = !mute;

    if (micMuted)
        microphone->stop();
    else
        // Start with given buffer
        microphone->start(audioCapture);

    // emit StatusMessageRequested(micMuted ? QString("Mute mic") : QString("Unmte mic"), 1000);
}

void MediaManager::onMuteSound(bool mute)
{
    soundMuted = !mute;

    // Enable player but only once
    if (!soundMuted && player->state() != QAudio::ActiveState)
        player->start(audioPlayback);
    else
        player->stop();

    // emit StatusMessageRequested(soundMuted ? QString("Mute sound") : QString("Unmte sound"), 1000);

    // Debug
    // view->debugMode(!mute);  // Will be moved to menu or something
}

void MediaManager::onMuteCam(bool mute)
{
    camMuted = !mute;

    if (camMuted)
        camera->stop();
    else
        camera->start();
}

void MediaManager::onNewVideoData(QImage data)
{
    display->setPixmap(QPixmap::fromImage(data));

    static QElapsedTimer timer;
    auto time = timer.restart();

    // qDebug() << time;

    if (time > 1000)
        view->adjustSize();
}

void MediaManager::onNewAudioData(QByteArray data)
{
    // Write data to output buffer
     audioPlayback->write(data);
     audioPlayback->seek(audioPlayback->size() - data.size()); // Don't forget to move pointer back
}

void MediaManager::onCameraStateChanged()
{
    // If no settings yet and camera loaded
    if (cameraSettings == nullptr && camera->state() == QCamera::LoadedState)
    {
        // Get supported resolutions
        QList<QSize> resolutions =  camera->supportedViewfinderResolutions();

        if (resolutions.isEmpty())
            return;

        // Get smallest resolution
        QSize maxRes = resolutions.first();

        // Set resolution to camera
        cameraSettings = new QCameraViewfinderSettings();
        cameraSettings->setResolution(maxRes);
        camera->setViewfinderSettings(*cameraSettings);
    }
}

void MediaManager::onReadyForCapture(bool state)
{
    if(state == true)
    {
       camera->searchAndLock();
       capture->capture();
       camera->unlock();
    }
}

void MediaManager::onImageCaptured(int id, const QImage &image)
{
//    QElapsedTimer timer;
//    timer.start();

    emit newVideoData(const_cast<QImage&>(image));

//    qDebug() << timer.elapsed() << "msec";
}

void MediaManager::onInputBufferWritten(qint64 bytes)
{
//    static QElapsedTimer timer;
//    qDebug() << timer.restart() << "msec";

    // Read data from input buffer
    audioCapture->seek(audioCapture->size() - bytes);
    auto data = audioCapture->readAll();

    // Send to network manager
    emit newAudioData(data);
}

void MediaManager::onOutputBufferReady()
{
    // Turn player back on when new data available
    if (player->state() == QAudio::IdleState)
        player->start(audioPlayback);
}
