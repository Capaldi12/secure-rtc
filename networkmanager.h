#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QObject>
#include <QMainWindow>
#include <QUdpSocket>
#include <QTcpSocket>

#include "encryptionprovider.h"

class NetworkManager : public QObject
{
    Q_OBJECT

private:
    enum MessageType
    {
        Handshake,
        Confirm,
        Decline,
        Goodbye,
        Audio,
        Video,
        Text,
        Other,
    };

public:
    explicit NetworkManager(QMainWindow *parent = nullptr);

    friend QDataStream &operator<<(QDataStream &out, NetworkManager::MessageType &msg);
    friend QDataStream &operator>>(QDataStream &in, NetworkManager::MessageType &msg);

private:

    QUdpSocket* udpSocket = nullptr;
    QTcpSocket* tcpSocket = nullptr;

    EncryptionProvider *ep = nullptr;

    int udpPort = 50000;

    int tcpPort = 50000;

    QHostAddress host;
    int port = 50000;

    bool isInitiator = false;
    bool ok;

    void makeSocket();
    void connectSocket();
    void destroySocket();

    bool onLine();

    void processPendingDatagrams();

    // Message sending
    void sendHandshake();
    void sendConfirm();
    void sendDecline();
    void sendGoodbye();
    void sendAudio(QByteArray &data);
    void sendVideo(QImage &data);
    void sendText(QString &data);

    // Message handling
    void handleHandshake(QNetworkDatagram &datagram, QDataStream &stream);
    void handleConfirm(QDataStream &stream);
    void handleDecline();
    void handleGoodbye();
    void handleAudio(QByteArray &data);
    void handleVideo(QByteArray &data);
    void handleText(QByteArray &data);

    void _endCall(bool sendGoodbye);

public slots:
    void checkBind();

    // Call control
    void startCall(QString ip, QString passphrase);
    void endCall();
    void acceptCall(QString passphrase);
    void declineCall();

    // Data sending
    void onNewVideoData(QImage data);
    void onNewAudioData(QByteArray data);
    void onTextMessage(QString msg);

private slots:
    void onUdpSocketReadyRead();
    void onTcpSocketReadyRead();

    void disconnectLater();

signals:
    // Call events
    void callStarted();
    void callEnded();
    void incomingCall(QString ip);
    void errorOccured(QString errorMsg);

    // New data recieved
    void newVideoData(QImage data);
    void newAudioData(QByteArray data);
    void textMessage(QString msg);

    // Sent when widget has something to say
    void StatusMessageRequested(QString msg, int timeout);
};

#endif // NETWORKMANAGER_H
