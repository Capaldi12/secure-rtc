#ifndef SCALABLEVIDEOVIEW_H
#define SCALABLEVIDEOVIEW_H

#include <QObject>
#include <QResizeEvent>

#include <QGraphicsView>
#include <QGraphicsScene>

#include <QGraphicsPixmapItem>

class ScalableVideoView : public QGraphicsView
{
    Q_OBJECT

public:
    ScalableVideoView(QWidget *parent);

    void setDisplayItem(QGraphicsPixmapItem *item);
    QGraphicsPixmapItem *removeDisplayItem();

    void adjustSize();

private:
    QGraphicsScene *scene;  // Current scene

    QGraphicsPixmapItem *item = nullptr;  // Displayed video item

    void resizeDisplayItem(QSize vSize);

    void resizeEvent(QResizeEvent *event) override;

    void scrollContentsBy(int, int) override {} // Disable scrolling

};

#endif // SCALABLEVIDEOVIEW_H
