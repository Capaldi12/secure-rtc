#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QRegExpValidator>
#include <QMessageBox>
#include <QInputDialog>
#include <QTime>
#include <QListWidgetItem>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    disableAllButtons();
    setupValidators();

    // Создание медиа менеджера
    mm = new MediaManager(ui->display, this);

    // Отобразить сообщение от менеджера в строке состояния
    connect(mm, &MediaManager::StatusMessageRequested,
            ui->statusbar, &QStatusBar::showMessage);

    // Проверка вводимых данных
    connect(ui->ipEdit, &QLineEdit::textChanged, this, &MainWindow::validateInput);
    connect(ui->passwordEdit, &QLineEdit::textChanged, this, &MainWindow::validateInput);

    // Включение/отключение медиа
    connect(ui->muteMicroButton, &QCheckBox::toggled, this, &MainWindow::muteMic);
    connect(ui->muteSoundButton, &QCheckBox::toggled, this, &MainWindow::muteSound);
    connect(ui->disableCameraButton, &QCheckBox::toggled, this, &MainWindow::muteCam);

    // Начало вызова
    connect(ui->callButton, &QPushButton::clicked, this, &MainWindow::onStartCallPressed);
    connect(ui->callButton, &QPushButton::clicked, this, &MainWindow::onCallStarted);

    // Завершение вызова
    connect(ui->endCallButton, &QPushButton::clicked, this, &MainWindow::callEnd);
    connect(ui->endCallButton, &QPushButton::clicked, this, &MainWindow::onCallEnded);

    // Управление медиа
    connect(this, &MainWindow::muteMic, mm, &MediaManager::onMuteMic);
    connect(this, &MainWindow::muteSound, mm, &MediaManager::onMuteSound);
    connect(this, &MainWindow::muteCam, mm, &MediaManager::onMuteCam);

    // Создания сетевого менеджера
    nm = new NetworkManager();
    nm->moveToThread(&netwrokThread);

    connect(&netwrokThread, &QThread::finished, nm, &QObject::deleteLater);

    // Отобразить сообщение от менеджера в строке состояния
    connect(nm, &NetworkManager::StatusMessageRequested,
            ui->statusbar, &QStatusBar::showMessage);

    // Управление вызовом
    connect(this, &MainWindow::callStart, nm, &NetworkManager::startCall);
    connect(this, &MainWindow::callEnd, nm, &NetworkManager::endCall);
    connect(this, &MainWindow::callAccept, nm, &NetworkManager::acceptCall);
    connect(this, &MainWindow::callDecline, nm, &NetworkManager::declineCall);

    // Обратная связь
    connect(nm, &NetworkManager::callStarted, this, &MainWindow::onCallStarted);
    connect(nm, &NetworkManager::callEnded, this, &MainWindow::onCallEnded);
    connect(nm, &NetworkManager::incomingCall, this, &MainWindow::onIncomingCall);
    connect(nm, &NetworkManager::errorOccured, this, &MainWindow::onError);

    // Получение данных
    connect(nm, &NetworkManager::newAudioData, mm, &MediaManager::onNewAudioData);
    connect(nm, &NetworkManager::newVideoData, mm, &MediaManager::onNewVideoData);

    // Отправка данных
    connect(mm, &MediaManager::newAudioData, nm, &NetworkManager::onNewAudioData);
    connect(mm, &MediaManager::newVideoData, nm, &NetworkManager::onNewVideoData);

    // Чат
    connect(ui->sendButton, &QPushButton::clicked, this, &MainWindow::onSendMessage);
    connect(ui->messageEdit, &QLineEdit::returnPressed, this, &MainWindow::onSendMessage);

    connect(this, &MainWindow::textMessage, nm, &NetworkManager::onTextMessage);
    connect(nm, &NetworkManager::textMessage, this, &MainWindow::onTextMessage);

    netwrokThread.start();

    emit muteMic(ui->muteMicroButton->isChecked());
    emit muteSound(ui->muteSoundButton->isChecked());
    emit muteCam(ui->disableCameraButton->isChecked());
}

MainWindow::~MainWindow()
{
    netwrokThread.exit();
    netwrokThread.wait();

    delete ui;
}

void MainWindow::setupValidators()
{
    // Создаем строку для регулярного выражения
    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    // Создаем регулярное выражение с применением строки, как повторяющегося элемента
    QRegExp ipRegex ("^" + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange + "$");
    // Создаем валидатор регулярного выражения с применением созданного регулярного выражения
    QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
    // Устанавливаем валидатор на QLineEdit
    ui->ipEdit->setValidator(ipValidator);
}

void MainWindow::disableAllButtons()
{
    ui->callButton->setEnabled(false);
    ui->endCallButton->setEnabled(false);

    ui->messageEdit->setEnabled(false);
    ui->sendButton->setEnabled(false);
}

void MainWindow::displayMesage(QString msg, bool own)
{
    QString text = "";

    text += QTime::currentTime().toString("(hh:mm)");

    if (own)
        text += " Вы: ";
    else
        text += " Собеседник: ";

    text += msg;

    QListWidgetItem *item = new QListWidgetItem(text);

    ui->messageList->addItem(item);

    ui->messageList->scrollToItem(item);
}

void MainWindow::validateInput()
{
    if(ui->passwordEdit->text().length() >= 5 && ui->ipEdit->hasAcceptableInput())
        ui->callButton->setEnabled(true);
    else
        ui->callButton->setEnabled(false);
}

void MainWindow::onIncomingCall(QString ip)
{
    auto accept = QMessageBox::question(this, "Входящий вызов", "Входящий вызов от: " + ip + ". Принять?");
    if (accept == QMessageBox::Yes)
    {
        bool ok;
        QString password = QInputDialog::getText(this, "Пароль", "Введите пароль: ", QLineEdit::Normal, "", &ok);

        if (ok)
        {
            emit callAccept(password);
            return;
        }
    }

    emit callDecline();
}

void MainWindow::onCallStarted()
{
    ui->callButton->setEnabled(false);
    ui->endCallButton->setEnabled(true);

    ui->ipEdit->setEnabled(false);
    ui->passwordEdit->setEnabled(false);

    ui->messageEdit->setEnabled(true);
    ui->sendButton->setEnabled(true);

    ui->messageList->clear();
}

void MainWindow::onCallEnded()
{
    ui->endCallButton->setEnabled(false);
    validateInput();

    ui->ipEdit->setEnabled(true);
    ui->passwordEdit->setEnabled(true);

    ui->messageEdit->setEnabled(false);
    ui->sendButton->setEnabled(false);
}

void MainWindow::onError(QString error)
{
    QMessageBox::critical(this, "Ошибка!", error);
}

void MainWindow::onStartCallPressed()
{
    emit callStart(ui->ipEdit->text(), ui->passwordEdit->text());
}

void MainWindow::onSendMessage()
{
    QString msg = ui->messageEdit->text();
    if (msg.isEmpty())
        return;

    emit textMessage(msg);

    displayMesage(msg, true);

    ui->messageEdit->clear();
}

void MainWindow::onTextMessage(QString msg)
{
    displayMesage(msg, false);
}

