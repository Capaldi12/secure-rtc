#include "networkmanager.h"

#include <QInputDialog>
#include <QNetworkDatagram>
#include <QTimer>
#include <QBuffer>

QDataStream &operator<<(QDataStream &out, NetworkManager::MessageType &msg)
{
    out << (int)msg;
    return out;
}

QDataStream &operator>>(QDataStream &in, NetworkManager::MessageType &msg)
{
    int r;
    in >> r;
    msg = (NetworkManager::MessageType) r;
    return in;
}

NetworkManager::NetworkManager(QMainWindow *parent)
    : QObject(parent)
{
    // Testing
    // udpPort = QInputDialog::getInt(parent, "Порт", "Введите порт", 50000, 0, 65535);

    // Create socket and bind it to port
    udpSocket = new QUdpSocket(this);
    ok = udpSocket->bind(QHostAddress::AnyIPv4, (quint16)udpPort);

    QTimer::singleShot(100, this, &NetworkManager::checkBind);

    // Socket actions handling
    connect(udpSocket, &QUdpSocket::readyRead, this, &NetworkManager::onUdpSocketReadyRead);
}

void NetworkManager::makeSocket()
{
    destroySocket();

    tcpSocket = new QTcpSocket(this);
    tcpSocket->bind();

    tcpPort = tcpSocket->localPort();
}

void NetworkManager::connectSocket()
{
    tcpSocket->connectToHost(host, port);

    if (tcpSocket->waitForConnected())
    {
        emit StatusMessageRequested("Connection established", 5000);

        connect(tcpSocket, &QTcpSocket::readyRead, this, &NetworkManager::onTcpSocketReadyRead);
    }
    else
    {
        emit errorOccured(QString("Unable to establish connection:\n%1").arg(tcpSocket->errorString()));

        destroySocket();
    }
}

void NetworkManager::destroySocket()
{
    if (tcpSocket)
    {
        if (tcpSocket->state() == QTcpSocket::ConnectedState)
            tcpSocket->disconnectFromHost();

        tcpSocket->deleteLater();
        tcpSocket = nullptr;
    }
}

bool NetworkManager::onLine()
{
    return tcpSocket && tcpSocket->state() == QTcpSocket::ConnectedState;
}

void NetworkManager::processPendingDatagrams()
{
    while (udpSocket->hasPendingDatagrams())
    {
        // Get datagramm
        QNetworkDatagram datagram = udpSocket->receiveDatagram();
        QDataStream stream(datagram.data());

        // Check type
        MessageType type;
        stream >> type;

        // Handle different type of datagrams
        switch (type)
        {
        case Handshake: handleHandshake(datagram, stream); break;
        case Confirm:   handleConfirm(stream);   break;
        case Decline:   handleDecline();   break;

        default: break;
        }
    }
}

void NetworkManager::sendHandshake()
{
    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);

    stream << Handshake << tcpPort;

    udpSocket->writeDatagram(buffer, host, udpPort);
}

void NetworkManager::sendConfirm()
{
    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);

    stream << Confirm << tcpPort;

    udpSocket->writeDatagram(buffer, host, udpPort);
}

void NetworkManager::sendDecline()
{
    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);

    stream << Decline;

    udpSocket->writeDatagram(buffer, host, udpPort);
}

void NetworkManager::sendGoodbye()
{
    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);

    stream << Goodbye;

    tcpSocket->write(buffer);
}

void NetworkManager::sendAudio(QByteArray &data)
{
    ep->encrypt(data);

    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);

    stream << Audio;
    stream << data;

    tcpSocket->write(buffer);
}

void NetworkManager::sendVideo(QImage &data)
{
    // Should fix all strange channel swapping
    if (data.format() != QImage::Format_RGB32)
        data = data.convertToFormat(QImage::Format_RGB32);

    QByteArray img;
    QDataStream sImg(&img, QIODevice::WriteOnly);

    sImg << data;

    ep->encrypt(img);

    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);

    stream << Video << img;

    tcpSocket->write(buffer);
}

void NetworkManager::sendText(QString &data)
{
    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);

    QByteArray text = data.toUtf8();

    ep->encrypt(text);

    stream << Text << text;

    tcpSocket->write(buffer);
}

void NetworkManager::handleHandshake(QNetworkDatagram &datagram, QDataStream &stream)
{
    // Only when not connected and not waiting for answer
    if (!onLine() && !isInitiator)
    {
        // Get host address
        host = datagram.senderAddress();

        stream >> port;

        // Anounce incoming call
        emit incomingCall(host.toString());
        emit StatusMessageRequested(QString("Incoming call from %1...").arg(host.toString()), 5000);
    }
}

void NetworkManager::handleConfirm(QDataStream &stream)
{
    emit StatusMessageRequested("Host replied, staring call...", 5000);

    stream >> port;

    connectSocket();
}

void NetworkManager::handleDecline()
{
    destroySocket();

    emit StatusMessageRequested("Host declined, ending call...", 5000);

    _endCall(false);
}

void NetworkManager::handleGoodbye()
{
    StatusMessageRequested("Host ended call...", 5000);

    _endCall(false);
}

void NetworkManager::handleAudio(QByteArray &data)
{
    ep->decrypt(data);

    emit newAudioData(data);
}

void NetworkManager::handleVideo(QByteArray &data)
{
    ep->decrypt(data);

    QImage img;
    QDataStream sImg(data);

    sImg >> img;

    emit newVideoData(img);
}

void NetworkManager::handleText(QByteArray &data)
{
    ep->decrypt(data);

    QString text = QString::fromUtf8(data);

    emit textMessage(text);
}

void NetworkManager::_endCall(bool sendGoodbye)
{
    isInitiator = false;

    emit StatusMessageRequested("Disconnecting...", 5000);

    if (sendGoodbye)
        this->sendGoodbye();

    // Close connection
    QTimer::singleShot(100, this, &NetworkManager::disconnectLater);
}

void NetworkManager::checkBind()
{
    if (ok)
        emit StatusMessageRequested(QString("Port %1 bound").arg(udpPort), 5000);
    else
        emit errorOccured("Не удалось связать порт!");
}

void NetworkManager::startCall(QString ip, QString passphrase)
{
    ep = new EncryptionProvider(passphrase);

    isInitiator = true;

    host = QHostAddress(ip);

    makeSocket();
    sendHandshake();

    emit StatusMessageRequested("Awaiting answer...", 5000);
}

void NetworkManager::endCall()
{
    if (onLine())
        _endCall(true);
}

void NetworkManager::acceptCall(QString passphrase)
{
    ep = new EncryptionProvider(passphrase);

    emit StatusMessageRequested("Answering call...", 5000);

    makeSocket();

    sendConfirm();

    connectSocket();

    emit callStarted();
}

void NetworkManager::declineCall()
{
    sendDecline();

    emit StatusMessageRequested("Call declined", 5000);
}

void NetworkManager::onNewVideoData(QImage data)
{
    if (onLine())
        sendVideo(data);
}

void NetworkManager::onNewAudioData(QByteArray data)
{
    if (onLine())
        sendAudio(data);
}

void NetworkManager::onTextMessage(QString msg)
{
    if (onLine())
        sendText(msg);
}

void NetworkManager::onUdpSocketReadyRead()
{
    processPendingDatagrams();
}

void NetworkManager::onTcpSocketReadyRead()
{
    QDataStream stream(tcpSocket);

    // Transaction in case of incomplete data
    stream.startTransaction();

    // Read type
    MessageType t;
    stream >> t;

    if (t == Goodbye)
        handleGoodbye();    // End call

    else if (t == Audio)
    {
        // Play audio
        QByteArray a;
        stream >> a;

        if (!stream.commitTransaction())
            return;

        handleAudio(a);
        return;
    }
    else if (t == Video)
    {
        // Display video
        QByteArray v;
        stream >> v;

        if (!stream.commitTransaction())
            return;

        handleVideo(v);
        return;
    }
    else if (t == Text)
    {
        // Display text
        QByteArray t;
        stream >> t;

        if (!stream.commitTransaction())
            return;

        handleText(t);
        return;
    }
    else
    {
        emit StatusMessageRequested(QString("Strange data received - %1, size: %2")
                                    .arg(t).arg(tcpSocket->bytesAvailable()), 5000);
    }

    stream.commitTransaction();
}

void NetworkManager::disconnectLater()
{
    destroySocket();

    emit StatusMessageRequested("Call ended", 5000);
    emit callEnded();
}
